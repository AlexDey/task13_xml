import java.util.List;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 23 Dec 2018
 * Parsing xml
 */
public class Application {
    private static List<Device> list;

    public static void main(String[] args) {
        new DOMParser().parseXML();
        initList();
    }

    // parse xml and add device objects to the list
    private static void initList() {
        list = new DOMParser().parseIntoList();
//        list.forEach(n -> System.out.println(n.getName()));
    }
}
