public class Type {
    private boolean peripheral;
    private int energyConsumption;
    private boolean isCooler;
    private boolean isUSB;
    private boolean isCom;
    private boolean isLTP;

    Type(boolean peripheral, int energyConsumption, boolean isCooler,
         boolean isUSB, boolean isCom, boolean isLTP) {
        this.peripheral = peripheral;
        this.energyConsumption = energyConsumption;
        this.isCooler = isCooler;
        this.isUSB = isUSB;
        this.isCom = isCom;
        this.isLTP = isLTP;
    }

    public void setPeripheral(boolean peripheral) {
        this.peripheral = peripheral;
    }

    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public void setCooler(boolean cooler) {
        isCooler = cooler;
    }

    public void setUSB(boolean USB) {
        isUSB = USB;
    }

    public void setCom(boolean com) {
        isCom = com;
    }

    public void setLTP(boolean LTP) {
        isLTP = LTP;
    }

    public boolean isPeripheral() {
        return peripheral;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    public boolean isCooler() {
        return isCooler;
    }

    public boolean isUSB() {
        return isUSB;
    }

    public boolean isCom() {
        return isCom;
    }

    public boolean isLTP() {
        return isLTP;
    }
}
