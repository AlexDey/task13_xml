public class Device {
    private String name;
    private Origin origin;
    private int price;
    private Type type;
    private boolean critical;

    Device(String name, Origin origin, int price, Type type, boolean cricital) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.type = type;
        this.critical = cricital;
    }

    String getName() {
        return name;
    }

    public Origin getOrigin() {
        return origin;
    }

    public int getPrice() {
        return price;
    }

    public Type getType() {
        return type;
    }

    public boolean isCricital() {
        return critical;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setCricital(boolean critical) {
        this.critical = critical;
    }
}
