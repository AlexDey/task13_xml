import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

class DOMParser {
    //Get Document Builder
    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private DocumentBuilder builder;

    {
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    //Build Document
    private Document document;

    {
        try {
            assert builder != null;
            document = builder.parse(new File("devices.xml"));
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    void parseXML() {
        //Normalize the XML Structure
        document.getDocumentElement().normalize();

        //Get all devices
        NodeList nList = document.getElementsByTagName("device");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                //Print each device's detail
                Element eElement = (Element) node;
                System.out.println("Device id: " + eElement.getAttribute("id"));
                System.out.println("Name: " + eElement.getElementsByTagName("Name")
                        .item(0).getTextContent());
                System.out.println("Origin: " + eElement.getElementsByTagName("Origin")
                        .item(0).getTextContent());
                System.out.println("Peripheral: " + eElement.getElementsByTagName("Peripheral")
                        .item(0).getTextContent());
                System.out.println("Energy consumption: " + eElement.getElementsByTagName("EnergyConsumption")
                        .item(0).getTextContent());
                System.out.println("Cooler: " + eElement.getElementsByTagName("Cooler")
                        .item(0).getTextContent());
                System.out.println("USB: " + eElement.getElementsByTagName("USB")
                        .item(0).getTextContent());
                System.out.println("COM: " + eElement.getElementsByTagName("COM")
                        .item(0).getTextContent());
                System.out.println("LPT: " + eElement.getElementsByTagName("LPT")
                        .item(0).getTextContent());
            }
            System.out.println();    //Just a separator
        }
    }

    List<Device> parseIntoList() {
        List<Device> list = new ArrayList<>();
        Device device = null;
        Type type = null;

        //Normalize the XML Structure
        document.getDocumentElement().normalize();

        //Get all devices
        NodeList nList = document.getElementsByTagName("device");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                //Print each device's detail
                Element eElement = (Element) node;
                String name = eElement.getElementsByTagName("Name")
                        .item(0).getTextContent();
                eElement.getElementsByTagName("Origin")
                        .item(0).getTextContent();
                int price = Integer.parseInt((eElement.getElementsByTagName("Price")
                        .item(0).getTextContent()));
                boolean peripheral = Boolean.parseBoolean((eElement.getElementsByTagName("Peripheral")
                        .item(0).getTextContent()));
                int consumption = Integer.parseInt(eElement.getElementsByTagName("EnergyConsumption")
                        .item(0).getTextContent());
                boolean cooler = Boolean.parseBoolean(eElement.getElementsByTagName("Cooler")
                        .item(0).getTextContent());
                boolean usb = Boolean.parseBoolean(eElement.getElementsByTagName("USB")
                        .item(0).getTextContent());
                boolean com = Boolean.parseBoolean(eElement.getElementsByTagName("COM")
                        .item(0).getTextContent());
                boolean lpt = Boolean.parseBoolean(eElement.getElementsByTagName("LPT")
                        .item(0).getTextContent());
                boolean critical = Boolean.parseBoolean(eElement.getElementsByTagName("Critical")
                        .item(0).getTextContent());

                type = new Type(peripheral, consumption, cooler, usb, com, lpt);
                list.add(new Device(name, Origin.USA, price, type, critical));
            }
        }
        return list;
    }
}
