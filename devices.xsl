<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Computer Devices</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Origin</th>
                        <th>Price</th>
                        <th>Critical</th>
                        <th>Energy Consumption</th>
                        <th>Peripheral</th>
                        <th>Cooler</th>
                        <th>USB</th>
                        <th>COM</th>
                        <th>LPT</th>
                    </tr>
                    <xsl:for-each select="devices/device">
                        <xsl:sort select="Name"/>
                        <tr>
                            <td>
                                <xsl:value-of select="Name"/>
                            </td>
                            <td>
                                <xsl:value-of select="Origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="Price"/>
                            </td>
                            <td>
                                <xsl:value-of select="Critical"/>
                            </td>
                            <td>
                                <xsl:value-of select="Type/EnergyConsumption"/>
                            </td>
                            <td>
                                <xsl:value-of select="Type/Peripheral"/>
                            </td>
                            <td>
                                <xsl:value-of select="Type/Cooler"/>
                            </td>
                            <td>
                                <xsl:value-of select="Type/USB"/>
                            </td>
                            <td>
                                <xsl:value-of select="Type/COM"/>
                            </td>
                            <td>
                                <xsl:value-of select="Type/LPT"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>